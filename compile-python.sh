#!/bin/bash




_get_last_tag()
{
    [[ $1 =~ ^[23.]\.[0-9]+\.[0-9]+$ ]] && echo $1 || \
        curl -s https://www.python.org/ftp/python/ \
        | grep -o ">[0-9.]\+/<" \
        | sed "s/^>//;s|/<$||" \
        | grep ^$1 \
        | sort --version-sort \
        | tail -n 1
}

_get_last_instable()
{
    [[ $1 =~ ^[23.]\.[0-9]+\.0$ ]] && \
        curl -s https://www.python.org/ftp/python/$1/ \
        | grep -o "Python-[0-9]\.[0-9]\+\.0[a-z0-9]\+" \
        | sort \
        | uniq \
        | sed "s/Python-[0-9.]*//;" \
        | sort --version-sort \
        | tail -n 1
}

compile-python()
{
    # Inspired from the great https://gitlab.com/python-devs/ci-images/
    # Thanks Barry Warsaw.

    # Needs:
    # sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

    local PY_VERSION=`_get_last_tag $1`
    local BETA=${2:-`_get_last_instable $PY_VERSION`}
    local URL="https://www.python.org/ftp/python"
    (
        cd /tmp || return 1
        [ -f $HOME/.local/bin/python${PY_VERSION}${BETA} ] || return 2
        wget -qO- "$URL/$PY_VERSION/Python-$PY_VERSION$BETA.tgz" | tar -xzf - || (
            echo "Version not found, check on $URL."
        )
        [ -d "Python-$PY_VERSION$BETA" ] && (cd "Python-$PY_VERSION$BETA"; ./configure --prefix="$HOME/.local/" && make -j "$(nproc)" && make altinstall) &&
            rm -r "Python-$PY_VERSION$BETA" &&
            cp $HOME/.local/bin/python${PY_VERSION%.*} $HOME/.local/bin/python${PY_VERSION}${BETA}
    )
}

compile-pythons()
{
    # Compiles a usefull set of Python versions.
    compile-python 3.7.17 &
    compile-python 3.8.18 &
    compile-python 3.9.18 &
    compile-python 3.10.13 &
    compile-python 3.11.5 &
    compile-python 3.12.0 &
    compile-python 3.13.0 a2 &
    wait
}

_compile_python()
{
    COMPREPLY=( $( compgen -W '$( command curl -s https://www.python.org/ftp/python/  | grep -o ">[0-9.]\+/<" | sed "s/^>//;s|/<$||" )' -- "${COMP_WORDS[COMP_CWORD]}") )
}
complete -F _compile_python compile-python
